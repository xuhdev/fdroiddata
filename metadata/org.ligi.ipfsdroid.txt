Categories:System
License:GPLv3
Web Site:https://github.com/ligi/IPFSDroid/blob/HEAD/README.md
Source Code:https://github.com/ligi/IPFSDroid
Issue Tracker:https://github.com/ligi/IPFSDroid/issues

Auto Name:IPFSDroid
Summary:IPFS Tool
Description:
This App does not represent a full IPFS node yet! Think of it as a dependency
injection for IPFS on android. Currently we use the centralized service ipfs.io
so you can use ipfs on android - later this will be exchanged for a full-node
for decentralisation but all things that use this app don't have to change/care.
.

Repo Type:git
Repo:https://github.com/ligi/IPFSDroid

Build:0.1,1
    commit=0.1
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/android-sdk-manager/d' build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1
Current Version Code:1
